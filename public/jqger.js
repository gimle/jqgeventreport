/**
 *
 * @licstart  The following is the entire license notice for the
 *  JavaScript code in this page.
 *
 * Copyright (C) 2015  Tux Solbakk
 *
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU GPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.
 *
 * @licend  The above is the entire license notice
 * for the JavaScript code in this page.
 *
 */

if (document.registerElement) {
	document.registerElement('jqger-main');
}

(function($) {
	$.jqgerhtml = function (selector, parent) {
		var events = $.jqger(selector, parent);

		var html = '<h1>' + events.total + ' events</h1><jqger-main>';

		if (events.duplicates.length > 0) {
			html += "<span style=\"color: #c00;\">Multiple id's detected!</span><br/>\n";
			for (i in events.duplicates) {
				html += events.duplicates[i]['num'] + ' #' + events.duplicates[i]['id'] + "<br/>\n";
			}
			html += "<br/>\n";
		}

		for (i in events.event) {
			html += i + "<br/>\n";
			for (j in events.event[i]) {
				html += events.event[i][j] + ' ' + j + "<br/>\n";
			}
			html += "<br/>\n";
		}

		html += '</jqger-main>';

		return html;
	};

	$.jqgertext = function (selector, parent) {
		var events = $.jqger(selector, parent);

		var text = events.total + " events\n\n";

		if (events.duplicates.length > 0) {
			text += "=== Multiple id's detected! ===\n";
			for (i in events.duplicates) {
				text += events.duplicates[i]['num'] + ' #' + events.duplicates[i]['id'] + "\n";
			}
			text += "\n";
		}

		for (i in events.event) {
			text += i + "\n";
			for (j in events.event[i]) {
				text += events.event[i][j] + ' ' + j + " handler\n";
			}
			text += "\n";
		}

		return text;
	};

	$.jqgerjson = function (selector, parent) {
		return JSON.stringify($.jqger(selector, parent));
	};

	$.jqger = function (selector, parent) {
		var report = {'duplicates': [], 'event': {}, 'total': 0};
		var windowEvents = $(window).data("events") || {};

		var total = 0;

		report.event = {};

		var list = function (events) {
			var ret = {};
			for (var name in events) {
				var current = events[name];

				if (current.delegateCount) {
					for (var i = 0; i < current.length; i++) {
						if (current[i].selector) {
							total++;

							var subName = name + ' for ' + current[i].selector;

							if (ret[subName] == undefined) {
								ret[subName] = 1;
							} else {
								ret[subName]++;
							}
						}
					}
				} else {
					total += current.length;
					if (ret[name] == undefined) {
						ret[name] = current.length;
					} else {
						ret[name] += current.length;
					}
				}
			}
			return ret;
		};

		$('[id]').each(function () {
			var ids = $('[id="' + this.id + '"]');
			if ((ids.length > 1) && (ids[0] == this)) {
				report.duplicates.push({'id': this.id, 'num': ids.length});
			}
		});

		if ((selector == undefined) || (selector == window)) {
			$(window, parent).addBack().each(function () {
				var events = $._data(this, 'events');
				if (!events) {
					return;
				}
				report.event['window'] = list(events);
			});
		}

		$(selector || '*', parent).addBack().each(function () {
			var events = $._data(this, 'events');
			if (!events) {
				return;
			}

			var name = '';
			if (this.tagName != undefined) {
				name += this.tagName;
			} else {
				name += 'document';
			}
			if (this.id) {
				name += '#' + this.id;
			}
			if (this.className) {
				name += '.' + this.className.replace(/ +/g, '.');
			}

			if ((selector == undefined) || (name != 'document') || (selector == document)) {
				var list2 = list(events);
				if (report.event[name] == undefined) {
					report.event[name] = {};
				}
				for (i in list2) {
					if (report.event[name][i] == undefined) {
						report.event[name][i] = list2[i];
					} else {
						report.event[name][i] += list2[i];
					}
				}
			}
		});

		report.total = total;

		return report;
	};
})(jQuery);
