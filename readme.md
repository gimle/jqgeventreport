jQuery Gimle Event Report
=========================
jQuery Gimle Event Report is a tool made to visualize all events. Example html is included.

In addition to the events reported it will also always report if a document contains duplicate id's.

Examples
--------

```html
<script>
	$.jqgerhtml(); // Output html formatted reports.
	$.jqgerjson(); // Output json formatted reports.
	$.jqgertext(); // Output text formatted reports.
</script>
```

The examples below all use html output and are meant to show possible arguments. All output methods supports the same arguments.

```html
<script>
	$.jqgerhtml(); // Outputs all events.
	$.jqgerhtml(document); // Outputs all events on the document.
	$.jqgerhtml('div'); // Outputs all events on any div in the document.
	$.jqgerhtml('div', '#dummyEvents'); // Outputs all events on any div in the document that is a child of #dummyEvents.
</script>
```
